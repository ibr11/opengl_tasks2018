#ifndef STUDENTTASKS2018_SHADER_H
#define STUDENTTASKS2018_SHADER_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <GL/glew.h>

class Shader {
public:
    GLuint Program;
    Shader(const GLchar* vertexPath, const GLchar* fragmentPath);
    void Use();
};

#endif STUDENTTASKS2018_SHADER_H
