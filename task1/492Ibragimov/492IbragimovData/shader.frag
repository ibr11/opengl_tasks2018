#version 330 core

in vec4 Normal;

out vec4 color;

void main() {
    color = 0.5f * Normal + 0.5f;
}