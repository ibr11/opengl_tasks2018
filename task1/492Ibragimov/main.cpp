#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <cmath>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Camera.hpp"

#include "shader.h"
#include "Camera.hpp"


GLFWwindow* InitWindow();
void GenAndBindObjects(GLuint* VAO, GLuint* VBO, GLuint* EBO);
void CreateCone(GLfloat radius, GLfloat height, GLuint n_sides);
void DrawTree(GLuint n_subtrees, Shader& shader, glm::vec3 pos = glm::vec3(0.0f),
              glm::mat4 rotation = glm::mat4(1.0), GLuint iter = 0);


void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
}


int main() {
    GLFWwindow* window = InitWindow();

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height);

    Shader my_shader("492IbragimovData/shader.vert", "492IbragimovData/shader.frag");

    GLuint VAO;
    GLuint EBO;
    GLuint VBO;
    GenAndBindObjects(&VAO, &VBO, &EBO);
    CreateCone(0.05f, 1.0f, 100);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);


    glfwSetKeyCallback(window, key_callback);
    OrbitCameraMover ocm;
    while(glfwWindowShouldClose(window) == 0) {
        glfwPollEvents();
        glClear(GL_COLOR_BUFFER_BIT);
        my_shader.Use();

        //glm::mat4 view;
        //view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));
        //glm::mat4 projection;
        //projection = glm::perspective(glm::radians(45.0f), GLfloat(width) / height, 0.1f, 100.0f);
        double xpos, ypos;
        glfwGetCursorPos(window, &xpos, &ypos);
        ocm.handleMouseMove(window, xpos, ypos);
        ocm.update(window, 0.01);
        glm::mat4 view = ocm.cameraInfo().viewMatrix;
        glm::mat4 projection = ocm.cameraInfo().projMatrix;

        GLint viewLoc = glGetUniformLocation(my_shader.Program, "view");
        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        GLint projectionLoc = glGetUniformLocation(my_shader.Program, "projection");
        glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

        glBindVertexArray(VAO);
        DrawTree(4, my_shader);
        glBindVertexArray(0);

        glfwSwapBuffers(window);
    }


    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
    glfwTerminate();

    return 0;
}


GLFWwindow* InitWindow() {
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);


    GLFWwindow* window = glfwCreateWindow(800, 600, "Tree", nullptr, nullptr);
    glfwMakeContextCurrent(window);


    glewExperimental = GL_TRUE;
    glewInit();
    return window;
}


void GenAndBindObjects(GLuint* VAO, GLuint* VBO, GLuint* EBO) {
    glGenVertexArrays(1, VAO);
    glGenBuffers(1, VBO);
    glGenBuffers(1, EBO);

    glBindVertexArray(*VAO);
    glBindBuffer(GL_ARRAY_BUFFER, *VBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *EBO);
}


void CreateCone(GLfloat radius, GLfloat height, GLuint n_sides) {
    GLuint n_vertices = n_sides + 1;
    GLfloat slant = GLfloat(sqrt(radius * radius + height * height));

    GLuint indices[3 * n_sides];

    GLfloat verticesX[n_vertices];
    GLfloat verticesY[n_vertices];
    GLfloat verticesZ[n_vertices];

    GLfloat normalX[n_vertices];
    GLfloat normalY[n_vertices];
    GLfloat normalZ[n_vertices];

    verticesX[0] = 0;
    verticesY[0] = height;
    verticesZ[0] = 0;

    normalX[0] = 0;
    normalY[0] = 1;
    normalZ[0] = 0;



    for (GLuint i = 1; i < n_vertices; ++i) {
        verticesX[i] = GLfloat(radius * cos(i * 2 * M_PI / n_sides));
        verticesY[i] = 0;
        verticesZ[i] = GLfloat(radius * sin(i * 2 * M_PI / n_sides));

        normalX[i] = GLfloat(height * cos(i * 2 * M_PI / n_sides) / slant);
        normalY[i] = GLfloat(radius / slant);
        normalZ[i] = GLfloat(height * sin(i * 2 * M_PI / n_sides) / slant);

        indices[3 * (i - 1)] = 0;
        indices[3 * (i - 1) + 1] = i % n_sides;
        indices[3 * (i - 1) + 2] = (i + 1) % n_sides;
    }

    GLfloat coneAttributes[n_vertices * 6];
    for (GLuint i = 0; i < n_vertices; ++i) {
        coneAttributes[i * 6] = verticesX[i];
        coneAttributes[i * 6 + 1] = verticesY[i];
        coneAttributes[i * 6 + 2] = verticesZ[i];
        coneAttributes[i * 6 + 3] = normalX[i];
        coneAttributes[i * 6 + 4] = normalY[i];
        coneAttributes[i * 6 + 5] = normalZ[i];
    }

    glBufferData(GL_ARRAY_BUFFER, sizeof(coneAttributes), coneAttributes, GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
}

void DrawTree(GLuint n_subtrees, Shader& shader, glm::vec3 pos, glm::mat4 rotation, GLuint iter) {
    if (iter < n_subtrees) {
        glm::mat4 model = glm::translate(glm::mat4(1.0), pos) * rotation;

        GLint modelLoc = glGetUniformLocation(shader.Program, "model");
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
        glDrawElements(GL_TRIANGLES, 300, GL_UNSIGNED_INT, 0);

        glm::vec3 x_axis = glm::vec3(1, 0, 0);
        glm::vec3 y_axis = glm::vec3(0, 1, 0);
        glm::vec3 z_axis = glm::vec3(0, 0, 1);
        GLfloat angle_fwd = glm::radians(45.0f);
        GLfloat angle_back = glm::radians(-45.0f);

        DrawTree(n_subtrees, shader,
                 pos + glm::mat3(glm::scale(rotation, glm::vec3(0.3f))) * y_axis,
                 glm::rotate(glm::scale(rotation, glm::vec3(0.7f)), angle_fwd, z_axis), iter + 1);
        DrawTree(n_subtrees, shader,
                 pos + glm::mat3(glm::scale(rotation, glm::vec3(0.3f))) * y_axis,
                 glm::rotate(glm::scale(rotation, glm::vec3(0.7f)), angle_back, z_axis), iter + 1);
        DrawTree(n_subtrees, shader,
                 pos + glm::mat3(glm::scale(rotation, glm::vec3(0.3f))) * y_axis,
                 glm::rotate(glm::scale(rotation, glm::vec3(0.7f)), angle_fwd, x_axis), iter + 1);
        DrawTree(n_subtrees, shader,
                 pos + glm::mat3(glm::scale(rotation, glm::vec3(0.3f))) * y_axis,
                 glm::rotate(glm::scale(rotation, glm::vec3(0.7f)), angle_back, x_axis), iter + 1);
    }
}
